class AddRefineryUserIndexOnSpreeApi < ActiveRecord::Migration
  def change
    add_index :refinery_users, :spree_api_key
  end
end
