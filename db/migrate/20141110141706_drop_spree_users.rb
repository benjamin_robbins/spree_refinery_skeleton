class DropSpreeUsers < ActiveRecord::Migration
  def up
    drop_table :spree_users
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
