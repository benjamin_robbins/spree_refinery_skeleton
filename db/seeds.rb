# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Added by Refinery CMS Pages extension
Refinery::Pages::Engine.load_seed

# Added by Refinery CMS Blog engine
Refinery::Blog::Engine.load_seed

Spree::Core::Engine.load_seed if defined?(Spree::Core)

# This engine loads a default admin if one is not present
#Spree::Auth::Engine.load_seed if defined?(Spree::Auth) 

ref_user = Refinery::User.find_or_create_by(email: 'ben.robbins@robinsolutionsllc.com') do |user|
  user.username = "Ben"
  user.full_name = "Ben Robbins"
  user.password = "secret21"
  user.password_confirmation = "secret21"
end
ref_user.add_role(:refinery)
ref_user.add_role(:superuser)
ref_user.plugins = Refinery::Plugins.registered.in_menu.names
ref_user.add_spree_role('admin')

ref_user = Refinery::User.find_or_create_by(email: 'holly@hkcreationsindiana.com') do |user|
  user.username = "Holly"
  user.full_name = "Holly Robbins"
  user.password = "secret21"
  user.password_confirmation = "secret21"
end
ref_user.add_role(:refinery)
ref_user.add_role(:superuser)
ref_user.plugins = Refinery::Plugins.registered.in_menu.names
ref_user.add_spree_role('admin')



