source 'https://rubygems.org'

gem 'rails', '4.1.7'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc

gem 'rack-cache'
gem 'unicorn'
gem 'pg'

# Assets
gem 'bootstrap-sass', '~> 3.2.0'
gem 'font-awesome-rails'
gem "autoprefixer-rails"

# Refinery CMS core
gem 'refinerycms', git: 'https://github.com/refinery/refinerycms', branch: 'master'
gem 'refinerycms-i18n', git: 'https://github.com/refinery/refinerycms-i18n', branch: 'master'
gem 'refinerycms-acts-as-indexed', ['~> 2.0', '>= 2.0.0']
gem 'refinerycms-wymeditor', ['~> 1.0', '>= 1.0.0']
gem 'seo_meta', git: 'https://github.com/parndt/seo_meta', branch: 'master'
gem 'paper_trail', git: 'https://github.com/airblade/paper_trail', branch: 'master'


# See the README for install instructions
# Refinery Addons
gem 'refinerycms-settings', git: 'https://github.com/refinery/refinerycms-settings', branch: 'master'
gem 'refinerycms-blog', git: 'https://github.com/refinery/refinerycms-blog', branch: 'master'
gem 'refinerycms-page-images',  git: 'https://github.com/refinery/refinerycms-page-images', branch: 'master'

gem 'refinery-spree', git: 'https://github.com/robin-solutions/spree-refinery', branch: 'master'

# Spree addons

gem 'spree', git: 'https://github.com/spree/spree', branch: '2-4-stable'
gem 'spree_gateway', git: 'https://github.com/spree/spree_gateway', branch: '2-4-stable'
gem 'spree_bootstrap', git: 'https://github.com/robin-solutions/spree_bootstrap', branch: 'master'
gem 'spree_auth_devise_bootstrap', git: 'https://github.com/robin-solutions/spree_bootstrap_auth_devise', branch: 'refinery-spree'
gem 'spree_paypal_express', github: 'spree-contrib/better_spree_paypal_express', branch: '2-4-stable'

# S3 image storage
gem 'aws-sdk'

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_21]
  gem 'quiet_assets'
  gem 'bullet', '4.14.0'
  gem 'spring'
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'capistrano', '3.1.0'
  gem 'capistrano-rails', '1.1.0'
  gem 'capistrano-bundler', '1.1.3'
  gem 'capistrano-rbenv', '2.0.2'
end

