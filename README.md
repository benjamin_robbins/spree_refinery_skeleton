# Refinery/Spree baseline project

## Version
Rails 4.1.7

Ruby 2.1.2

## Basic install

Clone the repo, then run bundle update to resolve the dependencies.


### Uses
Refinery 3.0.0

Spree 2.4.0.rc3

Bootstrap 3

### Deploy

We use Capistrano to deploy the app. Sane defaults were put into place, but there are some files that need edited per application.

#### Deploy setup
1. Edit `config/deploy.rb`. Should only need to change the top of the file
2. Edit `config/deploy/production.rb` file to change the server settings

*IMPORTANT* 
PROD_DATABASE_PW environment variable must be set for the db setup task to complete

Review `deploy/shared/secrets.yml.erb` to verify that the right environment variables are set.

#### Capistrano commands

##### Cold deploys (1 time commands)

1. `cap production deploy:setup_config` 
   setup_config is hooked to create the shared app configuration, copy the SSL certs, and create the initial database
2. `cap production deploy`
3. `cap production database:seed` 

##### Further deploys

After the one-time commands are run, then make sure the git branch is up to date (local changes are pushed), then run the command below to deploy the website

`cap production deploy`

### Secrets
See the `config/secrets.yml` file for the secrets that are used.

Common secrets:

1. secret_key_base
2. Mandrill settings for email
3. Facebook app identifiers for FB login
4. Mailchimp settings for Mailchimp newsletters 


### Configuration settings that need adjusted
#### ActionMailer
Edit `config/environments/production.rb` to set the ActionMailer settings. By default, email is sent by SMTP via Mandrill with the credentials from secrets.yml. If you stick with mandrill, then update the secrets.yml and you're good to go
