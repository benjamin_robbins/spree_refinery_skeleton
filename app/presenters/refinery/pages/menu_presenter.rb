require 'active_support/core_ext/string'
require 'active_support/configurable'
require 'action_view/helpers/tag_helper'
require 'action_view/helpers/url_helper'

module Refinery
  module Pages
    class MenuPresenter
      include ActionView::Helpers::TagHelper
      include ActionView::Helpers::UrlHelper
      include ActiveSupport::Configurable

      config_accessor :roots, :menu_tag, :list_tag, :list_item_tag, :css, :dom_id,
                      :max_depth, :selected_css, :first_css, :last_css, :list_tag_css,
                      :link_tag_css, :list_tag_dropdown_css, :is_toplevel_menu
      self.dom_id = 'collapsing-navigation'
      self.css = 'collapse navbar-collapse'
      self.menu_tag = :div
      self.list_tag = :ul
      self.list_item_tag = :li
      self.selected_css = :active
      self.first_css = nil
      self.last_css = nil
      self.list_tag_css = 'nav navbar-nav navbar-right'
      self.list_tag_dropdown_css = "dropdown"
      self.is_toplevel_menu = true

      def roots
        config.roots.presence || collection.roots
      end

      attr_accessor :context, :collection
      delegate :output_buffer, :output_buffer=, :to => :context

      def initialize(collection, context)
        @collection = collection
        @context = context
      end

      def to_html
        render_menu(roots) if roots.present?
      end

      private
      def render_menu(items)
        content_tag(menu_tag, :id => dom_id, :class => css) do
          render_menu_items(items)
        end
      end

      def render_menu_items(menu_items)
       # binding.pry
        if menu_items.present?
          if is_toplevel_menu?
            self.is_toplevel_menu = false
            render_toplevel_menu_items(menu_items)
          else
            render_dropdown_menu(menu_items)
          end
        end
      end

      def render_toplevel_menu_items(menu_items)
        content_tag(list_tag, :class => list_tag_css) do
          secondary_buffer = ActiveSupport::SafeBuffer.new
          secondary_buffer = menu_items.each_with_index.inject(ActiveSupport::SafeBuffer.new) do |buffer, (item, index)|
            buffer << render_menu_item(item, index)
          end

          #add the spree cart link to the end of the menu

          secondary_buffer << content_tag(list_item_tag, :id => 'link-to-cart', :data => { :hook => true }) do
            content_tag(:noscript) do
              link_to Spree.t(:cart), context.spree.cart_path 
            end << '&nbsp;'.html_safe
          end

          #secondary_buffer << content_tag(:script, 'if (window.Spree) {Spree.fetch_cart()}')
          secondary_buffer
        end
      end

      # <li id="link-to-cart" data-hook>
      #   <noscript>
      #     <%= link_to Spree.t(:cart), spree.cart_path %>
      #   </noscript>
      #   &nbsp;
      # </li>
      # <script>Spree.fetch_cart()</script>

      def render_dropdown_menu(menu_items, title)
        content_tag(list_item_tag, :class => list_tag_dropdown_css) do 
          buf = ActiveSupport::SafeBuffer.new 
          buf = content_tag(:a, :href => '#', :class => 'dropdown-toggle', :data => { :toggle => 'dropdown' } ) do  
              "#{title} <span class='caret'></span>".html_safe
          end

          buf << content_tag(list_tag, :class => 'dropdown-menu', :role => 'menu') do
            menu_items.each_with_index.inject(ActiveSupport::SafeBuffer.new) do |buffer, (item, index)|
              buffer << render_menu_item(item, index)
            end
          end

        end
      end

      def render_menu_item_link(menu_item)
        link_to(menu_item.title, context.refinery.url_for(menu_item.url), :class => link_tag_css)
      end

      def render_menu_item(menu_item, index)
        if menu_item.children.any?
          children = menu_item_children(menu_item)
          render_dropdown_menu(children, menu_item.title)
        else
          render_standard_menu_item(menu_item, index)
        end
      end

      def render_standard_menu_item(menu_item, index)
        content_tag(list_item_tag, :class => menu_item_css(menu_item, index)) do
          buffer = ActiveSupport::SafeBuffer.new
          buffer << render_menu_item_link(menu_item)
          buffer
        end
      end

      # Determines whether any item underneath the supplied item is the current item according to rails.
      # Just calls selected_item? for each descendant of the supplied item
      # unless it first quickly determines that there are no descendants.
      def descendant_item_selected?(item)
        item.has_children? && item.descendants.any?(&method(:selected_item?))
      end

      def selected_item_or_descendant_item_selected?(item)
        selected_item?(item) || descendant_item_selected?(item)
      end

      # Determine whether the supplied item is the currently open item according to Refinery.
      def selected_item?(item)
        path = context.request.path
        path = path.force_encoding('utf-8') if path.respond_to?(:force_encoding)

        # Ensure we match the path without the locale, if present.
        if %r{^/#{::I18n.locale}/} === path
          path = path.split(%r{^/#{::I18n.locale}}).last.presence || "/"
        end

        # First try to match against a "menu match" value, if available.
        return true if item.try(:menu_match).present? && path =~ Regexp.new(item.menu_match)

        # Find the first url that is a string.
        url = [item.url]
        url << ['', item.url[:path]].compact.flatten.join('/') if item.url.respond_to?(:keys)
        url = url.last.match(%r{^/#{::I18n.locale.to_s}(/.*)}) ? $1 : url.detect{|u| u.is_a?(String)}

        # Now use all possible vectors to try to find a valid match
        [path, URI.decode(path)].include?(url) || path == "/#{item.original_id}"
      end

      def menu_item_css(menu_item, index)
        css = []

        css << selected_css if selected_item_or_descendant_item_selected?(menu_item)
        css << first_css if index == 0
        css << last_css if index == menu_item.shown_siblings.length

        css.reject(&:blank?).presence
      end

      def menu_item_children(menu_item)
        within_max_depth?(menu_item) ? menu_item.children : []
      end

      def within_max_depth?(menu_item)
        !max_depth || menu_item.depth < max_depth
      end

      def is_toplevel_menu?
        self.is_toplevel_menu
      end

    end
  end
end
