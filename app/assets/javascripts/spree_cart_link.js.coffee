$ ->
  if window.Spree
    Spree.fetch_cart()
  else 
    $ltc = $('#link-to-cart')
    if $ltc.length > 0
      $.ajax
        url: '/store/cart_link',
        success: (data) ->
          $ltc.html data