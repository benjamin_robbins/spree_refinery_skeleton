# encoding: utf-8
Refinery::Core.configure do |config|
  # When true will rescue all not found errors and display a friendly error page
  config.rescue_not_found = Rails.env.production?

  # When true this will force SSL redirection in all Refinery backend controllers.
  config.force_ssl = true

  # When true will use Amazon's Simple Storage Service instead of
  # the default file system for storing resources and images
  config.s3_backend = true
  config.s3_bucket_name = Rails.application.secrets.s3_bucket_name # ENV['S3_BUCKET']
  config.s3_access_key_id = Rails.application.secrets.s3_access_key_id # ENV['S3_KEY']
  config.s3_secret_access_key = Rails.application.secrets.s3_secret_access_key # ENV['S3_SECRET']


  # Site name
  config.site_name = "HK Creations Indiana LLC"

  # This activates Google Analytics tracking within your website. If this
  # config is left blank or set to UA-xxxxxx-x then no remote calls to
  # Google Analytics are made.
  config.google_analytics_page_code = "UA-55365615-1"

  # Enable/disable authenticity token on frontend
  config.authenticity_token_on_frontend = true

  # Should set this if concerned about DOS attacks. See
  # http://markevans.github.com/dragonfly/file.Configuration.html#Configuration
  config.dragonfly_secret = "12965793b490caad7c85674b1e3bc4a1e5ba66a388e9e174"

  # Register extra javascript for backend
  # config.register_javascript "prototype-rails"

  # Register extra stylesheet for backend (optional options)
  # config.register_stylesheet "custom", :media => 'screen'

  # Specify a different backend path than the default of /refinery.
  config.backend_route = "cms"
end
