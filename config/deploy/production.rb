# require 'capistrano/rails'
# require 'capistrano/bundler'
app = File.join(File.expand_path('../..', __FILE__), 'application')
require app

#####################################
# Must change the server IP and roles
#####################################

server '104.236.37.163', user: 'deploy', roles: %w{web app db}, primary: true

# This is used in the Nginx VirtualHost to specify which domains
# the app should appear on. If you don't yet have DNS setup, you'll
# need to create entries in your local Hosts file for testing.
set :server_name, "www.hkcreationsindiana.com hkcreationsindiana.com"

# whether we're using ssl or not, used for building nginx
# config file
set :enable_ssl, true
set :ssl_certificate, File.join(Rails.root, 'config', 'deploy', 'ssl', 'ssl-bundle.crt')
set :ssl_certificate_key, File.join(Rails.root, 'config', 'deploy', 'ssl', 'hkcreations.key')

# number of unicorn workers, this will be reflected in
# the unicorn.rb and the monit configs
set :unicorn_worker_count, 4

set :stage, :production
set :branch, "master"

# used in case we're deploying multiple versions of the same
# app side by side. Also provides quick sanity checks when looking
# at filepaths
set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"
set :deploy_to, "/home/#{fetch(:deploy_user)}/apps/#{fetch(:full_app_name)}"

# dont try and infer something as important as environment from
# stage name.
set :rails_env, :production



